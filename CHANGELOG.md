## 2.3.1
Make `colorpicker` dependency `api` scoped since it's now part of the public API (`PredefinedColorPickerPreference`'s
dialog fragment)

## 2.3.0
- Make `AbstractToggleGroupPreference` and `SliderPreference` usable with hardware keys
- Fix `ToggleGroupPreference`'s toggle group being able to be visually unchecked (selection functionality
  is not affected)
- Deprecate summary provider on `MultiSelectToggleGroupPreference` and `ToggleGroupPreference`,
  remove `useSimpleSummaryProvider` attribute
- `PredefinedColorPickerPreference`'s color widget is now hidden when no value is set
- `PredefinedColorPickerPreference` also takes a color int in `setDefaultValue`
- Add slider value change listener to `SliderPreference`
- Add slider value display to `SliderPreference` that acts like `SeekBarPreference`'s
- Add `BlankPreference`
- Add separate `setIcons` method to `AbstractToggleGroupPreference`
- `PredefinedColorPickerPreference` now has its own dialog fragment - `PreferenceFragmentCompat.installConfigurationChangePatch`
  no longer covers it

## 2.2.5
Remove `*.Material2/3` styles, which was causing a bug in `custom-preferences-theming-integration`

## 2.2.4
- Change `AbstractToggleGroupPreference`'s entries data type to `List`
- Fix `MultiSelectToggleGroupPreference`'s `defStyleAttr` and `defStyleRes`-taking constructors not
  actually calling those parameters 

## 2.2.3
Extend value setter guard to `ToggleGroupPreference` and `TimePickerPreference`

## 2.2.2
- Remove `coreLibraryDesugaring` requirement
- Fix setting `value` in `SliderPreference.setProperties`
- Guard `SliderPreference` and `PredefinedColorPickerPreference`'s value setter to be a no-op when
  the new value is same as the old one

## 2.2.1
Bump color picker, which fixes a regression caused by 2.2.0 where `PredefinedColorPickerPreference`'s
dialog reappears when`Activity.recreate()` is called on `setOnPreferenceChangeListener`

## 2.2.0
- `TimePickerPreference`'s simple summary provider uses Android's time format method
- Make `TimePickerPreference`'s string-time conversion methods public
- Remove local color picker module, use standalone external library
- `PreferenceFragmentCompat.installConfigurationChangePatch` now also applies to `PredefinedColorPickerPreference`

## 2.1
- Add method for `PreferenceFragmentCompat` to make `TimePickerPreference`'s dialog not break on configuration changes
- Make `MultiSelectToggleGroupPreference`'s value non-null
- Make `SliderPreference.setProperties` take nullable values, add a builder version of it
- Add `require...` methods to preferences that have nullable values

## 2.0.2
Move Material 3 styles to `custom-preferences-theming-integration`

## 2.0.1
Fix issue where `PredefinedColorPickerPreference`'s dialog is retained when `Activity.recreate()`
is called in `setOnPreferenceChangeListener`

## 2.0
- Change project license from MIT to Apache 2.0
- Add `CanSetPreferenceChangeListener`
- Add `SliderPreference`, `ToggleGroupPreference`, `MultiSelectToggleGroupPreference`
- Use `androidx.preference`'s dialog fragment tag for `AbstractCustomDialogPreference.displayDialog`
- `PredefinedColorPickerPreferences` now uses a fork of [ColorPicker](https://github.com/Dhaval2404)
  included as a locally added module
- `PredefinedColorPickerPreference` uses `setColorFilter` instead of `setTint` to change color widget's
  color
- Change `custom-preferences-theming-integration` to use `theming-preference-integration` introduced
  with `theming` 2.0
- Move `custom-preferences-theming-integration` module to separate repository

## 1.1.1
Add all behavior of `PreferenceFragmentCompat.onDisplayPreferenceDialog` to `AbstractCustomDialogPreference.displayDialog`

## 1.1
- Re-add core library desugaring
- Make `androidx.preference` dependency have `api` scope
- Add ability to check illegal color values to `PredefinedColorPickerPreference` - if the current
  preference value isn't contained in the new values, an exception is thrown
- Add `getSimpleSummaryProvider` method to `PredefinedColorPickerPreference` and `TimePickerPreference`
- Make some methods and properties public
- Add methods to save and retrieve values used by `TimePickerPreference` from `SharedPreferences`
  and `PreferenceDataStore`
- Make all preference classes extendable

## 1.0
Initial release