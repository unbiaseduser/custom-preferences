package com.sixtyninefourtwenty.custompreferences

import android.content.Context
import android.content.res.TypedArray
import android.graphics.drawable.Drawable
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import androidx.core.view.children
import androidx.core.view.get
import androidx.preference.Preference
import com.google.android.material.button.MaterialButtonToggleGroup
import com.sixtyninefourtwenty.custompreferences.internal.getAndroidXNotSetString
import com.sixtyninefourtwenty.custompreferences.internal.throwValueNotSetException
import java.util.Objects

/**
 * [AbstractToggleGroupPreference] that allows users to select a single option. This preference
 * saves a string value (from one of [copyOfEntryValues]).
 */
@Suppress("unused", "MemberVisibilityCanBePrivate")
open class ToggleGroupPreference : AbstractToggleGroupPreference, CanSetPreferenceChangeListener<String> {

    @JvmOverloads
    constructor(context: Context, attrs: AttributeSet? = null) : super(context, attrs) {
        init(context.obtainStyledAttributes(attrs, R.styleable.ToggleGroupPreference))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context.obtainStyledAttributes(attrs, R.styleable.ToggleGroupPreference, defStyleAttr, 0))
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context.obtainStyledAttributes(attrs, R.styleable.ToggleGroupPreference, defStyleAttr, defStyleRes))
    }

    private fun init(ta: TypedArray) {
        fun initIcons(): List<Drawable?>? {
            val arrayRes = ta.getResourceId(R.styleable.ToggleGroupPreference_tgp_icons, 0)
            return if (arrayRes == 0) {
                null
            } else {
                context.resources.getIntArray(arrayRes).map { ContextCompat.getDrawable(context, it) }
            }
        }

        setEntries(
            entries = ta.getTextArray(R.styleable.ToggleGroupPreference_tgp_entries)?.toList().orEmpty(),
            entryValues = ta.getTextArray(R.styleable.ToggleGroupPreference_tgp_entryValues)?.toList().orEmpty(),
            icons = initIcons()
        )
        ta.recycle()
    }

    override fun setOnPreferenceChange(block: ((newValue: String) -> Boolean)?) {
        setTypedPreferenceChangeListener(block)
    }

    override val isPreferenceSingleSelection: Boolean = true
    private var _value: String? = null
    var value: String?
        get() = _value
        set(value) = setValueInternal(value, true)

    /**
     * Return the value this preference has.
     * @throws IllegalStateException if the value has not been set, either from a default value or from user input
     * @see value
     */
    fun requireValue() = value ?: throwValueNotSetException()

    private fun setValueInternal(value: String?, notifyChanged: Boolean) {
        if (!Objects.equals(this._value, value)) {
            this._value = value
            persistString(value)
            if (notifyChanged) {
                notifyChanged()
            }
        }
    }

    private fun setValueOnToggleGroup(
        value: String?,
        entryValues: List<CharSequence>?,
        toggleGroup: MaterialButtonToggleGroup
    ) {
        if (value == null || entryValues == null) {
            toggleGroup.clearChecked()
        } else {
            val valueIndex = entryValues.indexOf(value)
            if (valueIndex >= 0) {
                toggleGroup.check(toggleGroup[valueIndex].id)
            } else {
                toggleGroup.clearChecked()
            }
        }
    }

    private fun handleInput(toggleGroup: MaterialButtonToggleGroup, buttonIndex: Int) {
        val strEntryValue = entryValues[buttonIndex].toString()
        if (callChangeListener(strEntryValue)) {
            setValueInternal(strEntryValue, false)
        } else {
            val oldButtonIndex = value?.let { entryValues.indexOf(it) } ?: -1
            if (oldButtonIndex >= 0) {
                toggleGroup.check(toggleGroup[oldButtonIndex].id)
            } else {
                toggleGroup.clearChecked()
            }
        }
    }

    override fun bind(toggleGroup: MaterialButtonToggleGroup) {
        val entryValues = this.entryValues
        setValueOnToggleGroup(value, entryValues, toggleGroup)
        toggleGroup.children.forEachIndexed { index, view ->
            view.setOnClickListener {
                handleInput(toggleGroup, index)
            }
        }
    }

    override fun onGetDefaultValue(a: TypedArray, index: Int): Any? {
        return a.getString(index)
    }

    override fun onSetInitialValue(defaultValue: Any?) {
        value = getPersistedString(defaultValue as String?)
    }

    override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState()
        if (isPersistent) {
            return superState
        }

        return SavedState(superState).apply {
            value = this@ToggleGroupPreference.value
        }
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state !is SavedState) {
            super.onRestoreInstanceState(state)
            return
        }

        super.onRestoreInstanceState(state.superState)
        value = state.value
    }

    private class SavedState : AbstractToggleGroupPreference.SavedState {

        @JvmField
        var value: String? = null

        constructor(source: Parcel): super(source) {
            value = source.readString()
        }

        constructor(superState: Parcelable?): super(superState)

        override fun writeToParcel(dest: Parcel, flags: Int) {
            super.writeToParcel(dest, flags)
            dest.writeString(value)
        }

        companion object CREATOR : Parcelable.Creator<SavedState> {
            override fun createFromParcel(source: Parcel): SavedState = SavedState(source)
            override fun newArray(size: Int): Array<SavedState?> = arrayOfNulls(size)
        }

    }

    companion object {
        @JvmStatic
        @Deprecated(
            message = "Doesn't work on normal user input since inline preferences" +
                    " don't update the rest of the UI when that happens.",
            level = DeprecationLevel.ERROR
        )
        fun getSimpleSummaryProvider() = summaryProvider
        private val summaryProvider by lazy(LazyThreadSafetyMode.NONE) {
            Preference.SummaryProvider<ToggleGroupPreference> { preference ->
                val valueIndex = preference.value?.let {
                    preference.entryValues.indexOf(it)
                } ?: -1
                if (valueIndex >= 0) {
                    preference.entries[valueIndex]
                } else {
                    preference.getAndroidXNotSetString()
                }
            }
        }
    }

}