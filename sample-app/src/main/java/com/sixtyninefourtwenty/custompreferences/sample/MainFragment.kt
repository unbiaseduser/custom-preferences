package com.sixtyninefourtwenty.custompreferences.sample

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.preference.Preference
import androidx.preference.PreferenceCategory
import androidx.preference.PreferenceGroup
import androidx.preference.PreferenceManager
import androidx.preference.SwitchPreferenceCompat
import androidx.preference.forEach
import com.sixtyninefourtwenty.custompreferences.BlankPreference
import com.sixtyninefourtwenty.custompreferences.MultiSelectToggleGroupPreference
import com.sixtyninefourtwenty.custompreferences.PredefinedColorPickerPreference
import com.sixtyninefourtwenty.custompreferences.PreferenceFragmentCompatAccommodateCustomDialogPreferences
import com.sixtyninefourtwenty.custompreferences.SliderPreference
import com.sixtyninefourtwenty.custompreferences.TimePickerPreference
import com.sixtyninefourtwenty.custompreferences.ToggleGroupPreference
import com.sixtyninefourtwenty.custompreferences.installConfigurationChangePatch
import com.sixtyninefourtwenty.custompreferences.sample.databinding.BlankPreferenceContentBinding

class MainFragment : PreferenceFragmentCompatAccommodateCustomDialogPreferences() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        installConfigurationChangePatch()
        val context = requireContext()
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        preferenceScreen = preferenceManager.createPreferenceScreen(context).apply {
            addPreference(SwitchPreferenceCompat(context).apply {
                key = "pcpp_group_toggle"
                title = "Toggle ${PredefinedColorPickerPreference::class.java.simpleName} group"
                setOnPreferenceChangeListener { _, newValue ->
                    findPreference<PreferenceGroup>("pcpp_group")!!.isVisible = newValue as Boolean
                    true
                }
            })
            addPreferenceCategory {
                key = "pcpp_group"
                title = PredefinedColorPickerPreference::class.java.simpleName
                isVisible = prefs.getBoolean("pcpp_group_toggle", false)
                addPreference(PredefinedColorPickerPreference(context).apply {
                    key = "pcpp1"
                    title = "Title"
                    summaryProvider = PredefinedColorPickerPreference.getSimpleSummaryProvider()
                })
            }
            addPreference(SwitchPreferenceCompat(context).apply {
                key = "tpp_group_toggle"
                title = "Toggle ${TimePickerPreference::class.java.simpleName} group"
                setOnPreferenceChangeListener { _, newValue ->
                    findPreference<PreferenceGroup>("tpp_group")!!.isVisible = newValue as Boolean
                    true
                }
            })
            addPreferenceCategory {
                key = "tpp_group"
                title = TimePickerPreference::class.java.simpleName
                isVisible = prefs.getBoolean("tpp_group_toggle", false)
                addPreference(TimePickerPreference(context).apply {
                    key = "tpp1"
                    title = "Title"
                    summaryProvider = TimePickerPreference.getSimpleSummaryProvider()
                })
            }
            addPreference(SwitchPreferenceCompat(context).apply {
                key = "tgp_group_toggle"
                title = "Toggle ${ToggleGroupPreference::class.java.simpleName} group"
                setOnPreferenceChangeListener { _, newValue ->
                    findPreference<PreferenceCategory>("tgp_group")!!.isVisible = newValue as Boolean
                    true
                }
            })
            addPreferenceCategory {
                key = "tgp_group"
                title = ToggleGroupPreference::class.java.simpleName
                isVisible = prefs.getBoolean("tgp_group_toggle", false)
                addPreference(ToggleGroupPreference(context).apply {
                    key = "tgp1"
                    title = "Title"
                    summary = "Basic"
                    setEntries(
                        entries = listOf("Option 1", "Option 2", "Option 3"),
                        entryValues = listOf("1", "2", "3")
                    )
                })
                addPreference(ToggleGroupPreference(context).apply {
                    key = "tgp2"
                    title = "Title"
                    summary = "With icons"
                    setEntries(
                        entries = listOf("Option 1", "Option 2", "Option 3"),
                        entryValues = listOf("1", "2", "3"),
                        icons = listOf(
                            ContextCompat.getDrawable(context, R.drawable.home),
                            null,
                            ContextCompat.getDrawable(context, R.drawable.search)
                        )
                    )
                })
                addPreference(ToggleGroupPreference(context).apply {
                    key = "tgp3"
                    title = "Title"
                    summary = "Rejects the second option"
                    setEntries(
                        entries = listOf("Option 1", "Option 2", "Option 3"),
                        entryValues = listOf("1", "2", "3")
                    )
                    setOnPreferenceChange {
                        if (it == entryValues[1]) {
                            return@setOnPreferenceChange false
                        }
                        true
                    }
                })
            }
            addPreference(SwitchPreferenceCompat(context).apply {
                key = "mstgp_group_toggle"
                title = "Toggle ${MultiSelectToggleGroupPreference::class.java.simpleName} group"
                setOnPreferenceChangeListener { _, newValue ->
                    findPreference<PreferenceCategory>("mstgp_group")!!.isVisible = newValue as Boolean
                    true
                }
            })
            addPreferenceCategory {
                key = "mstgp_group"
                title = MultiSelectToggleGroupPreference::class.java.simpleName
                isVisible = prefs.getBoolean("mstgp_group_toggle", false)
                addPreference(MultiSelectToggleGroupPreference(context).apply {
                    key = "mstgp1"
                    title = "Title"
                    summary = "Basic"
                    setEntries(
                        entries = listOf("Option 1", "Option 2", "Option 3"),
                        entryValues = listOf("1", "2", "3")
                    )
                })
                addPreference(MultiSelectToggleGroupPreference(context).apply {
                    key = "mstgp2"
                    title = "Title"
                    summary = "With icons"
                    setEntries(
                        entries = listOf("Option 1", "Option 2", "Option 3"),
                        entryValues = listOf("1", "2", "3"),
                        icons = listOf(
                            ContextCompat.getDrawable(context, R.drawable.home),
                            null,
                            ContextCompat.getDrawable(context, R.drawable.search)
                        )
                    )
                })
                addPreference(MultiSelectToggleGroupPreference(context).apply {
                    key = "mstgp3"
                    title = "Title"
                    summary = "Rejects the second option"
                    setEntries(
                        entries = listOf("Option 1", "Option 2", "Option 3"),
                        entryValues = listOf("1", "2", "3")
                    )
                    setOnPreferenceChange {
                        if (entryValues[1] in it) {
                            return@setOnPreferenceChange false
                        }
                        true
                    }
                })
            }
            addPreference(SwitchPreferenceCompat(context).apply {
                key = "sp_group_toggle"
                title = "Toggle ${SliderPreference::class.java.simpleName} group"
                setOnPreferenceChangeListener { _, newValue ->
                    findPreference<PreferenceCategory>("sp_group")!!.isVisible = newValue as Boolean
                    true
                }
            })
            addPreferenceCategory {
                key = "sp_group"
                title = SliderPreference::class.java.simpleName
                isVisible = prefs.getBoolean("sp_group_toggle", false)
                addPreference(SliderPreference(context).apply {
                    key = "sp1"
                    title = "Title"
                    summary = "Basic"
                })
                addPreference(SliderPreference(context).apply {
                    key = "sp2"
                    title = "Title"
                    summary = "With slider value display"
                    sliderValueFunction = { it.toInt().toString() }
                })
                addPreference(SliderPreference(context).apply {
                    key = "sp3"
                    title = "Title"
                    summary = "Rejects values greater than 6"
                    setOnPreferenceChange {
                        if (it > 6) {
                            return@setOnPreferenceChange false
                        }
                        true
                    }
                })
                addPreference(SliderPreference(context).apply {
                    key = "sp4"
                    title = "Title"
                    summary = "With slider value display and rejects values greater than 6"
                    sliderValueFunction = { it.toInt().toString() }
                    setOnPreferenceChange {
                        if (it > 6) {
                            return@setOnPreferenceChange false
                        }
                        true
                    }
                })
                addPreference(BlankPreference(context).apply {
                    key = "sp5_bp"
                    title = "Slider value"
                    setContentView { parent, attachToRoot ->
                        BlankPreferenceContentBinding.inflate(layoutInflater, parent, attachToRoot).root
                    }
                })
                addPreference(SliderPreference(context).apply {
                    key = "sp5"
                    title = "Title"
                    summary = "With slider value change listener and rejects values greater than 6"
                    addOnSliderValueChangeListener { value, fromUser ->
                        findPreference<Preference>("sp5_bp")!!.summary = "value: %d, fromUser: %b".format(value.toInt(), fromUser)
                    }
                    setOnPreferenceChange {
                        if (it > 6) {
                            return@setOnPreferenceChange false
                        }
                        true
                    }
                })
            }
        }
    }

    override fun onResume() {
        super.onResume()
        disableIconSpaceRecursively(preferenceScreen)
    }

    private fun disableIconSpaceRecursively(preference: Preference) {
        preference.isIconSpaceReserved = false
        if (preference is PreferenceGroup) {
            preference.forEach {
                disableIconSpaceRecursively(it)
            }
        }
    }

}