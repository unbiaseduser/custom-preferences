package com.sixtyninefourtwenty.custompreferences.sample

import androidx.preference.PreferenceCategory
import androidx.preference.PreferenceGroup

inline fun PreferenceGroup.addPreferenceCategory(block: PreferenceCategory.() -> Unit) {
    val category = PreferenceCategory(context)
    addPreference(category)
    block(category)
}